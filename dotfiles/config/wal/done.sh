#!/bin/sh
which deadd-notification-center && (killall deadd-notification-center; deadd-notification-center & disown)
python -m pywalfox && ~/.local/bin/pywalfox update
which pywal-discord && pywal-discord
find ~/.config/JetBrains/ -mindepth 1 -maxdepth 1 -exec ~/git/rlovelessiii/intellijPywal/intellijPywalGen.sh {} \;
sed -i 's/Opacity=1/Opacity=0.8/g' ~/.cache/wal/colors-konsole.colorscheme
